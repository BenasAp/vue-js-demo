export const namespaced = true;

export const state = {
  notifications: [],
};

let nextId = 1;

export const mutations = {
  PUSH(state, notification) {
    state.notifications.push({
      ...notification,
      id: nextId++,
    });
  },
  DELETE(state, notificationToRemove) {
    state.notifications = state.notifications.filter(
      notificatio => notificatio.id !== notificationToRemove.id
    );
  },
};

export const actions = {
  add({ commit }, notificatio) {
    commit('PUSH', notificatio);
  },
  remove({ commit }, notificationToRemove) {
    commit('DELETE', notificationToRemove);
  },
};
