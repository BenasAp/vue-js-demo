import EventService from '@/services/EventService';

export const state = {
  events: [],
  lastPage: 0,
  event: {},
};

export const mutations = {
  ADD_EVENT(state, event) {
    state.events.push(event);
  },
  SET_EVENTS(state, events) {
    state.events = events;
  },
  SET_LAST_PAGE(state, lastPage) {
    state.lastPage = lastPage;
  },
  SET_EVENT(state, event) {
    state.event = event;
  },
};

export const actions = {
  createEvent({ commit, dispatch }, event) {
    return EventService.postEvent(event)
      .then(() => {
        commit('ADD_EVENT', event);
        const notification = {
          type: 'success',
          message: 'Your event was created successfuly',
        };
        dispatch('notification/add', notification, { root: true });
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message: `There was a problem ${error.message}`,
        };
        dispatch('notification/add', notification, { root: true });
        throw error;
      });
  },
  fetchEvents({ commit, dispatch }, { perPage, page }) {
    EventService.getEvents(perPage, page)
      .then(res => {
        commit('SET_LAST_PAGE', res.headers['x-total-count']);
        commit('SET_EVENTS', res.data);
      })
      .catch(error => {
        console.log(error);
        const notification = {
          type: 'error',
          message: `There was a problem fetching events ${error.message}`,
        };
        dispatch('notification/add', notification, { root: true });
      });
  },
  fetchEvent({ commit, getters, dispatch }, id) {
    const event = getters.getEventById(id);

    if (event) {
      commit('SET_EVENT', event);
    } else {
      EventService.getEvent(id)
        .then(res => {
          commit('SET_EVENT', res.data);
        })
        .catch(errors => {
          const notification = {
            type: 'error',
            message: `There was a problem fetching event ${errors.message}`,
          };
          dispatch('notification/add', notification, { root: true });
        });
    }
  },
};

export const getters = {
  getEventById: state => id => {
    return state.events.find(event => event.id === id);
  },
};
